#ifndef CurrentColor_H
#define CurrentColor_H

#include <GL/freeglut.h>
#include "Rectangle.h"
#include "Color.h"

void CurrentColor(Color brushColor){
    Rectangle Outer(-0.97,-0.7, 0.34, 0.25, 0,0,0);
    Rectangle Inner(-0.96,-0.71, 0.32,0.23,1,1,1);
    Outer.draw();
    Inner.draw();

    Rectangle ColorboxOuter(-0.88,-0.85, 0.16, 0.1, 0,0,0);
    Rectangle ColorboxInner(-0.87,-0.86, 0.14, 0.08, brushColor);
    ColorboxOuter.draw();
    ColorboxInner.draw();

    glColor3f(0,0,0);
    glLineWidth(2);
    glBegin(GL_LINES);
    glVertex2f(-0.94,-0.75);
    glVertex2f(-0.9,-0.75);

    glVertex2f(-0.94,-0.75);
    glVertex2f(-0.94,-0.8);

    glVertex2f(-0.9,-0.8);
    glVertex2f(-0.94,-0.8);


    glVertex2f(-0.88,-0.75);
    glVertex2f(-0.84,-0.75);

    glVertex2f(-0.88,-0.75);
    glVertex2f(-0.88,-0.8);

    glVertex2f(-0.88,-0.8);
    glVertex2f(-0.84,-0.8);

    glVertex2f(-0.84,-0.75);
    glVertex2f(-0.84,-0.8);


    glVertex2f(-0.82,-0.75);
    glVertex2f(-0.82,-0.8);

    glVertex2f(-0.82,-0.8);
    glVertex2f(-0.78,-0.8);


    glVertex2f(-0.76,-0.75);
    glVertex2f(-0.72,-0.75);

    glVertex2f(-0.76,-0.75);
    glVertex2f(-0.76,-0.8);

    glVertex2f(-0.76,-0.8);
    glVertex2f(-0.72,-0.8);

    glVertex2f(-0.72,-0.75);
    glVertex2f(-0.72,-0.8);


    glVertex2f(-0.7,-0.75);
    glVertex2f(-0.7,-0.8);

    glVertex2f(-0.7,-0.7725);
    glVertex2f(-0.66,-0.8);

    // glVertex2f(-0.68,-0.8);
    // glVertex2f(-0.66,-0.8);
    

    glEnd();

//     glColor3f(0,0,0);
//     glLineWidth(2);
//     glBegin(GL_LINES);
//     glVertex2f(-0.75,-0.9);
//     glVertex2f(-0.7,-0.9);
//     glVertex2f(-0.7,-0.95);
//     glVertex2f(-0.75,-0.95);
//     glEnd();
}

#endif