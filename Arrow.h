#ifndef Arrow_H
#define Arrow_H

#include <GL/freeglut.h>
#include "Color.h"

struct Arrow{
    float w;
    float h;
    float x;
    float y;

    float r;
    float g;
    float b;

    Arrow(){
        x = 0;
        y = 0;
        w = 0.4;
        h = 0.2;
        r = 0;
        g = 0;
        b = 1;
    }

    Arrow(float x, float y, float w, float h, float r, float g, float b){
        this->x = x;
        this->y = y;
        this->w = w;
        this->h = h;
        this->r = r;
        this->g = g;
        this->b = b;
    }

    Arrow(float x, float y, float w, float h, Color color){
        this->x = x;
        this->y = y;
        this->w = w;
        this->h = h;
        this->r = color.r;
        this->g = color.g;
        this->b = color.b;
    }

    void draw(){
        glColor3f(r, g, b);
        glBegin(GL_POLYGON);

        glVertex2f(x, y);
        glVertex2f(x+w, y);
        glVertex2f(x+w, y-h);
        glVertex2f(x, y-h);

        glEnd();

        glBegin(GL_POLYGON);
        glVertex2f(x+w, y+(0.5*h));
        glVertex2f(x+w, y-h-(0.5*h));
        glVertex2f(x+w+(0.5*w), y-(0.5*h));
        glEnd();
    }
};

#endif