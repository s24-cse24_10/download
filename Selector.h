#ifndef SELECTOR_H
#define SELECTOR_H

#include <GL/freeglut.h>
#include <GL/gl.h>
#include "Rectangle.h"
#include "Arrow.h"


Rectangle Thickness[10];
Rectangle Outline;
Arrow ArrowSelector;

struct Selector{
    float w;
    float h;
    float x;
    float y;

    float r;
    float g;
    float b;

    Selector(){
        x = 0;
        y = 0;
        w = 0.34;
        h = 0.81;
        r = 0;
        g = 0;
        b = 0;
    }

    Selector(float x, float y){
        this->x = x;
        this->y = y;
        w = 0.34;
        h = 0.81;
        r = 0;
        g = 0;
        b = 0;
    }

    void draw(float current){
        Outline=Rectangle(x,y,w,h,r,g,b);
        Outline.draw();
        for (int i =0;i<=9;i++){
            Thickness[i]=Rectangle(x+0.01,(y-0.01)-(i*0.08),w-0.02,0.07,r+0.5,g+0.5,b+0.5);
            Thickness[i].draw();
        }

        for (int i=0; i<10; i++){
            glColor3f(0,0,0);
            glLineWidth(i+1);
            glBegin(GL_LINES);

            glVertex2f(x+0.1, y-0.045-(0.08*i));
            glVertex2f(x+w-0.02, y-0.045-(0.08*i));

            glEnd();
        }
        ArrowSelector=Arrow(x+0.02,y-0.03-(0.08*(current-1)),0.045,0.025,1,1,1);
        ArrowSelector.draw();

    }

    bool insideSelctor(float mx, float my){
        if (Outline.inside(mx,my)){
            return true;
        }
        else{
            return false;
        }
    }

    int SelectThickness(float mx, float my, float current){
        for (int i=0; i<10;i++){
            if (Thickness[i].inside(mx,my)){
                return i;
            }

        }
        return current;
    }

};

#endif