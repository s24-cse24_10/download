#include <cstdio>
#include <iostream>
#include <stdlib.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include "Rectangle.h"
#include "Button.h"
#include "Color.h"
#include "Point.h"
#include "Selector.h"
#include "Arrow.h"
#include "CurrentColor.h"

using namespace std;

// Window width and height
int width = 400;
int height = 400;


Point Paint[10000];
int brushSize =5;
int pCounter =0;
Color brushColor;

Rectangle canvas(-0.6,1, 1.4,1.75, 1, 1, 1);
Rectangle backgroundVertical(-1,1, 0.4,2, 0.8, 0.8, 0.8);
Rectangle backgroundHorizontal(-1,-0.75, 2,0.25, 0.8, 0.8, 0.8);
Rectangle Colorbackdrop(-0.6,-0.8,1.6,0.2, 0,0,0);

Rectangle White(-0.59,-0.81, 0.38,0.08, 1,1,1);
Rectangle Black(-0.59,-0.91, 0.38,0.08, 0,0,0);

Rectangle Red(-0.19,-0.81, 0.38,0.08, 1,0,0);
Rectangle Yellow(-0.19,-0.91, 0.38,0.08, 1,1,0);

Rectangle Green(0.21,-0.81, 0.38,0.08, 0,1,0);
Rectangle SkyBlue(0.21,-0.91, 0.38,0.08, 0,1,1);

Rectangle Blue(0.61,-0.81, 0.38,0.08, 0,0,1);
Rectangle Violet(0.61,-0.91, 0.38,0.08, 1,0,1);

Button clear("Clear", -0.95,0.45);

Button Eraser("Eraser", -0.95,0.70);

Button PaintBrush("Paint", -0.95,0.95, true);

Selector BrushSelector(-0.97,0.2);



// Convert window coordinates to Cartesian coordinates
void windowToScene(float& x, float& y) {
    x = (2.0f * (x / float(width))) - 1.0f;
    y = 1.0f - (2.0f * (y / float(height)));
}

void drawScene(){
    // Clear the screen and set it to current color (black)
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    // Objects to be drawn go here
    canvas.draw();
    for (int i=0;i<pCounter;i++){
        Paint[i].draw();
    }
    
    backgroundVertical.draw();
    backgroundHorizontal.draw();

    Colorbackdrop.draw();

    White.draw();
    Black.draw();

    Red.draw();
    Yellow.draw();

    Green.draw();
    SkyBlue.draw();

    Blue.draw();
    Violet.draw();

    clear.render();
    Eraser.render();
    PaintBrush.render();

    BrushSelector.draw(brushSize);
    
    CurrentColor(brushColor);

    // We have been drawing to the back buffer, put it in the front
    glutSwapBuffers();
}

void mouse(int button, int state, int x, int y) {
    /*
        button: 0 -> left mouse button
                1 -> right mouse button
        
        state:  0 -> mouse click
                2 -> mouse release
        
        x, y:   mouse location in window relative coordinates
    */
    float mx = x;
    float my = y;
    windowToScene(mx, my);

    if (button == 0 && state == 1){
        clear.down = false;
    }

    if (button == 0 && state == 0){
        if (canvas.inside(mx, my)){
            if (pCounter < 10000){
                Paint[pCounter] = Point(mx, my, brushColor, brushSize);
                pCounter++;
            }
        }
        else if (backgroundHorizontal.inside(mx, my)){
            if (White.inside(mx, my)){
                cout << "White was clicked" << endl;
                // r = 0; g = 0; b = 1;
                brushColor.set(1, 1, 1);
            }
            if (Red.inside(mx, my)){
                cout << "Red was clicked" << endl;
                brushColor.set(1,0,0);
            }
            if (Green.inside(mx, my)){
                cout << "Green was clicked" << endl;
                // r = 0; g = 0; b = 1;
                brushColor.set(0, 1, 0);
            }
            if (Blue.inside(mx, my)){
                cout << "Blue was clicked" << endl;
                // r = 0; g = 0; b = 1;
                brushColor.set(0, 0, 1);
            }
            if (Black.inside(mx, my)){
                cout << "Black was clicked" << endl;
                // r = 0; g = 0; b = 1;
                brushColor.set(0, 0, 0);
            }
            if (Yellow.inside(mx, my)){
                cout << "Yellow was clicked" << endl;
                // r = 0; g = 0; b = 1;
                brushColor.set(1, 1, 0);
            }
            if (SkyBlue.inside(mx, my)){
                cout << "Sky Blue was clicked" << endl;
                // r = 0; g = 0; b = 1;
                brushColor.set(0, 1, 1);
            }
            if (Violet.inside(mx, my)){
                cout << "Violet was clicked" << endl;
                // r = 0; g = 0; b = 1;
                brushColor.set(1, 0, 1);
            }
            
            
        }
        else if (backgroundVertical.inside(mx, my)){
            if (PaintBrush.inside(mx, my)){
                cout << "Paint Brush was clicked" << endl;
                PaintBrush.down = true;
                Eraser.down=false;
                brushColor.set(0,0,0);

            }
            if (Eraser.inside(mx, my)){
                cout << "Eraser was clicked" << endl;
                PaintBrush.down = false;
                Eraser.down = true;
                brushColor.set(1,1,1);
            }
            if (clear.inside(mx, my)){
                cout << "Clear was clicked" << endl;
                pCounter = 0;
                clear.down = true;
            }
            if (BrushSelector.insideSelctor(mx,my)){
                brushSize=BrushSelector.SelectThickness(mx,my,brushSize)+1;
                cout<<"Thickness level "<<brushSize<< " was clicked."<<endl;
            }
        }
    }
    glutPostRedisplay();
}

void motion(int x, int y) {
    /*
        x, y:   mouse location in window relative coordinates
    */
    float mx = x;
    float my = y;
    windowToScene(mx, my); 

    if (pCounter < 10000){
        if (canvas.inside(mx, my)){
            Paint[pCounter] = Point(mx, my, brushColor, brushSize);
            pCounter++;
        }
    }
    glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y) {
    /*
        key:    ASCII character of the keyboard key that was pressed
        x, y:   mouse location in window relative coordinates
    */
}

int main(int argc,char** argv) {
    // Perform some initialization
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);
    glutInitWindowSize(width, height);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("GLUT App");

    // Set the Display Function
    glutDisplayFunc(drawScene);

    // Set the Mouse Function
    glutMouseFunc(mouse);

    // Set the Motion Function
    glutMotionFunc(motion);

    // Set the Keyboard Funcion
    glutKeyboardFunc(keyboard);

    // Run the program
    glutMainLoop();

    return 0;
}